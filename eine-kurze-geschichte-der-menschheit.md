# Eine kurze Geschichte der Menschheit

Hier möchte ich Erkenntnisse, Fragen und Anmerkungen zu deisem Buch sammeln.

## Frage zu dem Begriff gemeinsamer Vorfahr"

"Alle Angehörigen einer Familie lassen sich auf einen gemeinsamen Urahn zurückführen"

Das hört sich so an, als wäre das ein einzelnes Tier gewesen. Ist das so gemeint?
Oder ist damit eine Tierart gemeint? Im weiteren Text wird es so beschrieben, dass es
tatsächlich ein einzelnes Tier war. Da die Änderungen zwischen Generationen ja aber gar
nicht so groß sind, könnte man dieses Tier ja gar nicht genau bestimmen; es muss eine
ganze Reihe von Müttern gewesen sein, deren Kinder sich jeweils ein kleines Stück vom
Rest der Art unterschieden haben. Eventuell mit langen Zeiträumen zwischen
diesen Änderungen.
