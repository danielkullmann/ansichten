#!/bin/sh

mkdir -p public

for f in *.md; do
    pandoc -s -o "public/${f%.md}.html" "$f"
    sed -i -e 's/.md"/.html"/g' "public/${f%.md}.html"
done
