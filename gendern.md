# Gendern

Ich verstehe ja, dass viele Menschen die Gendersprache nicht hübsch finden. Aber
kann das wirklich der einzige Grund dafür sein, es nicht zu tun? Ist es nicht
ein äußerst löbliches und zustimmungswürdiges Ziel, wenn man Sprache inklusiv
gestalten will? Damit sich wirklich alle mitgemeint fühlen können?

Wenn das so schwierig ist, könnte man ja das generische Feminin einführen, als
Alternative zum generischen Maskulin. Oder, um es auf die Spitze zu treiben, das
generische Neutrum. Also "das Bäckermeist" oder so.

So oder so, Sprache ist ja keine statische Angelegenheit, sie ändert sich
ständig. Muss sie ja auch, um sich an geänderte Bedingungen anzupassen.
