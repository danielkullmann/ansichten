# Beispeiel für meine Aussage, dass die Wahrheit immer in der Mitte liegt

## Politik

### Kapitalismus vs. Kommunismus

Ich bin kein Verfechter des Kaptialismus, aber ich glaube auch nicht, dass Kommunismus die Lösung ist.
Mit Kommunismus meine ich dabei die Einstellung, dass es kein Eigentum mehr geben soll, sondern Alles
Allen gehört.

Mein Haupteinwand gegen den Kommunismus ist die Erkenntnis, dass es sehr schwer ist, sich für allgemeines
Gut verantwortlich zu fühlen. Wenn niemand wirklich dafür zuständig ist, kümmert sich häufig auch niemend drum, bzw. kommen Menschen häufig zu dem Schluß, dass es das Problem von jemand Anderem ist.

Ein Beispiel dafür: Grünanlagen in Wohngegenden. Was könnte man für tolle Sachen mit Grünanlagen machen,
auch wenn sie sehr klein sind. Man könnte z.B. auf dem wenigen Raum Gärten anlegen, die z.B. gemeinsam
von den Anwohnern gepflegt werden. Aber da fängt es schon an: wenn alle dafür zuständig sind, ist eigentlich keiner dafür zuständig.

## IT

Dazu fallen mir z.B. die Diskussionen um "Clean Code" und zu "Micro Services" ein. In beiden Fällen plädieren
die Fürsprecher häufig zu sehr extremen Ansichten, die meiner Meinung nach aber überzogen sind. Ich meine aber auch gar nicht, dass diese Konzepte vollkommen daneben liegen, aber dass man Alles mit Augenmaß machen sollte,
und sich nicht jede Lösung auf jedes Problem anwenden lässt.


## Im Privaten

(Da habe ich gerade kein gutes Beispiel)
