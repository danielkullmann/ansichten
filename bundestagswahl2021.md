# Bundestagswahl 2021

Mann ey, es gibt ja keine Partei mehr, die man wirklich guten Gewissens wählen
kann. Alle Parteien sind schrecklich.

AfD: Eine Partei, die es akzeptiert, dass sich Rechtsradikale bei ihr einnisten,
kann man einfach nicht wählen. Auch wenn das nicht alles Rechtsradikale sind.

CDU/CSU: Das ist die "nichts ändert sich" Partei, die sich nur seehr langsam an
die Gegenwart anpasst. Wie lange hat es gedauert, bis sie vollständig veraltete
Sichtweisen endlich aufgegeben hat: Ihr Umgang mit Schwulen und Lesben, ihr
Festhalten an alten, letzendlich dem Untergang gewidmeten Industrien, ihre Weigerung,
den Klimawandel ernstzunehmen. Und zusätzlich dazu eine durch und durch verrottete
Partei, die sich mehr als 20 Jahre dagegen gesperrt hat, ihren massiven Parteispende-
skandel adäquat aufzuarbeiten. Und die ganzen Maskengewinnler, die zuletzt zurücktreten
mussten, sind doch nur die logische Konsequenz dieser Partei.

FDP: Ich finde den Menschenrechtshintergrund dieser Partei sehr gut; leider verschwindet
er vollständig hinter der neoliberalen Wirtschaftspolitik, und die alten Sprüche
("Leistung muss sich wieder lohnen") klingen nur hohl: wer sind denn eigentlich die 
Leistungsträger in unserer Gesellschaft. Das sind doch keineswegs die Wirtschaftsbosse
und Manager. Das sind die kleinen Betriebe, die Menschen beschäftigen und
Ausbildungsplätze schaffen. Das sind die Männer und Frauen, die sich neben der
Arbeit noch viel um ihre Kinder oder andere Angehörigen kümmern, oder sich für
gute Zwecke engagieren. Und eben auch die Leute, die während des Lockdowns arbeiten
mussten, in Supermärkten und Krankenhäusern. Auch die Fixierung auf "Freiheit"
ist letztendlich Augenwischerei: die FDP lehnt "Verbotspolitik" ab, aber letzlich ist
die CO2-Obergrenze, die sie propagiert, doch ein Verbot: wir dürfen nicht mehr als
das ausstoßen. Die Frage ist ja nicht, ob Verbote an sich schlecht sind, sondern
welche wir brauchen. Ich bin auch nicht für eine Politik, die sich hauptsächlich auf
Verbote stützt; aber die Alternative "der Markt wirds schon richten" bedeutet ja nur,
dass Dinge teurer werden, und dass ist der FDP und ihrer Klientel ja egal, weil die es
sich ja trotzdem leisten können; nur die "faulen" Armen müssen sich einschränken. Also
ist das ja auch im Ende eine Verbotspolitik, die halt schön als "freie Marktwirtschaft"
verpackt worden ist.

SPD: Diese Partei ist nur noch ein Witz. Olaf Scholz, wirklich? Der sollte eigentlich
damit beschäftigt sein, die Millionen an Steuernachzahlungen selbst zu bezahlen, die
er den Bewohnern von Hamburg vorenthalten hat. Die SPD mag zwar
Bergarbeiterlieder singen, was ich generell schon äußerst antiquiert finde, aber für
die Arbeiter hat die sich doch schon lange nicht mehr interessiert, außer wenn
sie die SPD wählen sollen. Leute wie Kevin Kühnert sind die einzige Hoffnung dieses
lahmen Vereins.

Grüne: Das ist die Partei, die ich wählen werde, aber leider nicht mit vollem Elan.
Die letzten Wahlkämpfe hatten diesen "Es muss sich was ändern, aber niemand muss
sich ändern" Vibe, der vollkommen daneben war. Klar, mit Themen wie dem Veggie-Tag
kriegt man immer noch massiv Gegenwind von den Leuten, die alles genauso lassen wollen
wie es ist. Aber es muss doch auch klar sein, dass sich was ändern muss, und was das dann
ist.

Die Linke: Ja, was soll man zu der Linken sagen? Sozial durchaus gute Ideen, aber Themen
wie Umweltschutz nehme ich ihr einfach nicht ab.

Kleinparteien: Das am häufigsten vorgetragene Argument gegen diese Parteien ist
ja, das man die leider nicht wählen kann, wenn man will, dass sich was in
Deutschland ändert, und dass demnach eine Wahl dieser Parteien leider eine
weggeworfene Stimme ist. Deshalb muss eigentlich ganz dringend das Wahlsystem
geändert werden, so dass diese Parteien eine echte Chance haben. Aber als
Protestpartei eignen sich die schon. Wenn die etablierten Parteien merken, dass
ihnen die Wähler weglaufen, müssen sie sich ja überlegen, was sie machen können.
Andererseits kann eine Partei ihr Programm ja nicht auf jeden potentiellen Wähler
anpassen, weil jeder ja unterschiedliche Ideen hat. Da sind auch Kompromisse gefragt.
Vielleicht macht es doch Sinn, das kleinere Übel zu wählen, mit dem man etwas
ändern kann, statt eine Partei zu wählen, die gar nicht in den Bundestag kommt.
