# Abtreibung

Kurz: Ich bin im Prinzip gegen Abtreibungen, aber für das Recht auf Abtreibung.

Das hört sich vielleicht merkwürdig an, aber ich kann es erklären: ich glaube,
es sollte so wenige Abtreibungen wie möglich geben. Eine Abtreibung ist eine
seelische und physische Belastung für den Körper der Frau, und sollte deshalb gut
überlegt sein. Andererseits bin ich der Meinung, dass eine Frau frei über ihren
Körper verfügen können sollte, und deshalb sollten Abtreibungen auch erlaubt
sein. Und gleichzeitig akzeptiere und unterstütze ich die Entscheidung der Frau.

Moralisch ist das keine so einfache Frage, weil es viele mögliche Ansichten zum
Recht zur Abtreibung geben kann. Für mich stellt sich die Lage folgendermassen
dar: ja, ein Embryo ist ein Leben in der Entstehung. Man kann sich lange darüber
streiten, wann genau dieses Leben nun anfängt, aber Eines ist klar: dieses sich 
im Entstehen befindene Leben ist doch sehr lange komplett vom Körper der werdenden
Mutter abhängig; medizinisch gesehen ist es nicht möglich, den Embryo vor der 24.
Schwangerschaftswoche ausserhalb des Körpers der Mutter am Leben zu erhalten.
Deshalb ist eine Regelung, nach der ein Schwangerschaftsabbruch z.B. bis zur 12.
Woche erlaubt sein sollte, für mich tragbar.

Und an die Leute gewendet, die nun behaupten, dass sei Mord und deshalb zu
verbieten: gbt es nach einer Fehlgeburt eine Ermttlung der Behörden, was passiert ist?
Sollte es eine geben? Sollte der Körper der Frau wg. Mordes angeklagt werden?

Klar ist es eine schwierige Frage, aber für mich ist es klar, dass das existierende
Leben als Wichtiger als das Entstehende Leben einzustufen ist; es ist ja noch nicht
mal klar, ob der Embryo von sich aus überleben würde.

Und ich glaube, dass auch die Abtreibungsgegner der Meinung sind, dass das ganze Thema
eine Grauzone ist. Fals ABtreibung Mord ist, bräuchten wir ja kein eigenes Gesetz dazu;
Mored ist ja schon verboten. Und wenn ein Embryo schon ein Mensch gibt, muss es nach
einer Fehlgeburt einen Totenschein geben? Sollte ein EMbryo schon eine Steuer-ID
bekommen (Kinder beommen ja schon nach der Geburt gleich eine Steuer-ID)? Weil ich das
von keinem Abtreibungsgegner höre. Und das ist für mich ein Indiz dafür, dass auch sie
erkennen, dass es eine Frage der Abstufung ist, und keine der klaren Grenze.

Ein weiteres Argument ist der Fakt, dass Kinder, die vor der 22. SSW als Frühgeburt
zur Welt kommen, im Allgemeinen als nicht lebensfähig betrachtet werden und normalerweise
niht versucht wird, sie am Leben zu erhalten.

Daher stelt sich dann die Frage: Sollten Schwangerschaftsabbrüche generell bis kurz vor
der Geburt erlaubt sein, oder bis wann? Für mich ist die Grenze erreicht, wenn das Kind
im Prinzip ohne die Mutter überleben kann, was zwischen der 23. und 25. SSW der Fall ist.


Auch die Frage, ob irgendein Gott uns da Vorschriften machen kann, ist für mich klar:
Da dieser Gott (falls es überhaupt existieren sollte) uns ja die Freiheiten
gegeben hat, selbst über unsere irdischen Angelegenheten zu entscheiden, sollte
es sich da auch heraushalten.

