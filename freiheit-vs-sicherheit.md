# Freiheit vs. Sicherheit

Mir scheint es, dass in vielen politischen Diskussionen zwischen diesen beiden
gegensätzlichen Zielen vermittelt werden muss.

Wenn es z.B. um die Gestaltung unserer Gesellschaft geht, sind die Konservativen
eher dafür, die alten Strukturen beizubehalten (Sicherheit), statt neue Dinge 
auszuprobieren (Freiheit). In der Diskussion über die beste Wirtschaftsform
präferieren Neoliberale die Freiheit des Marktes gegenüber der Sicherheit eines
sehr regulierten Systems (wo z.B. jemand viel schwieriger zu feuern ist).
Es ist auch nicht jeder Mensch in allen Bereichen eher der Sicherheitsecke oder 
der Freiheitsecke zuzuordnen; das kann je nach Thema wechseln.
 
