# Grundgedanken

## Pragmatismus

Pragmatismus ist mir wichtig im politischen Leben.

## Die Wahrheit liegt irgendwo in der Mitte

Im Laufe meines Lebens bin ich zu einer Grundeinsicht gekommen:
Die Wahrheit liegt (fast) nie in den Extremen, sondern (fast) immer irgendwo in der Mitte. Das heisst nicht, dass sie genau in der Mitte zwischen den Extremen liegt, aber doch sehr weit in der Mitte.

Dazu gibt es sehr viele Beispiele, nicht nur politische, sondern z.B. auch in der Informatik (meine Arbeitswelt), aber auch im Privaten. Beispiele dazu sammle ich auf [einer eigenen Seite](./beispiele-für-wahrheit-in-der-mitte.md).

## Mein Ziel: Das bestmögliche Leben für alle Menschen

Alles Andere muss sich diesem Ziel unterordnen. Z.B. muss das Wirtschaftssystem dieses Ziel unterstützen,
sonst müssen wir es soweit apasse, dass es das tut.

## Konsequenz

Ich mag es nicht, wenn jemandem fehlende Konsequenz vorgeworfen wird. Niemand ist in allen 
Bereichen komplett konsequent, und ausserdem kann man Ziele ja auch durch kleine Schritte erreichen.

Beispiel: Wenn ich Vegetarismus/Veganismus dem Fleischessen vorziehe, und trotzdem manchmal Fleisch esse,
bedeutet das nicht, dass meine Gründe unsinnig sind. Durch meinen reduzierten und bewussten Fleischkonsum
verbessere ich ja die Situation schon: weniger Tierleid, bessere Umweltbilanz, etc.

## Rechts vs. Links

Ich finde diese Unterscheidung zunehmend sinnlos, weil es nur Label sind, die mit bestimmten Vorurteilen belastet
sind. Viel wichtiger ist es mir, über tatsächliche Konzepte, Grundsätze und Lösungsansätze zu reden.
