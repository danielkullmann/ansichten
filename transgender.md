# Transgender 

Ist eine transgender Frau eine Frau, gamz ohne Einschränkung?

Das ist eine schwierige Frage, und ausserdem eine "loaded question", weil ein
"Nein" als Antwort oft als anti-Transgender-Aussage eingestuft wird. Aber die
Fragestellung ist doch deutlich komplizierter. Die erste Gegenfrage, die man
nämlich stellen sollte, ist: "was bedeutet es denn, eine Frau zu sein?" Das 
ist kein Ausweichen, sondern ein Hinweis darauf, dass es sehr viele unterschiedliche,
sich teilweise widersprechene Definitionen von "Frau" gibt. Es gibt biologische
Definitionen (Mehrzahl! es kann ja z.B. über die Gene defeniert werden, oder 
über den Hormonhaushalt, über Geschlechtsmerkmale oder sicher auch andere Anhaltspunkte),
aber auch gesellschaftliche Definitionen. 

Ich finde es erstmal wichtig, eine Person zu respektieren, und ebenso zu akzeptieren,
wie diese Person angesprochen werden will (ich vergleiche das oft mit dem Namen, mit dem eine
Person angesprochen werden will). Und genauso wichtig finde ich es, zu akzeptieren, wenn sich
jemand als "Frau" oder "Mann" oder irgendetwas anderes fühlt. Das widerspricht nicht der 
biologischen Realität, weil das Gefühl ja schon eine biologische Realität ist. Natürlich kann
man anmerken, dass die Person "biologisch" ein bestimmtes Geschlecht hat, und das stimmt ja auch
(auch wenn es, wie angedeutet, nicht so einfach ist, das Geschlecht so einfach biologisch
festzulegen), aber das Gefühl ist trotzdem auch eine Realität, und nur die Person selbst kann ja
sagen, wie es sich anfühlt, sich selbst zu sein. Da passt der Spruch "Fakten kümmern sich nicht 
um Deine Gefühle" einfach ganz und gar nicht.

## Anmerkung

Geschlecht als diese binäre "Mann" oder "Frau" festzulegen, geht an der biologischen Realität vorbei.
Einmal, weil es ja gar nicht unbedingt klar ist, welche Anhaltspunkte (Gene, Hormone, Geschlechtsmerkale)
man dazu nimmt. Zum Anderen, weil diese Anhaltspunkte gar nicht unbedingt binär sind. Was ist, wenn eine
Person zwar mänliche Hormone produziert, aber eben viel weniger als der Durchschnittsmann? Ist das dann 
nur noch zum Teil ein Mann?
