# Erziehung

Die Erziehung sollte das Ziel haben, Menschen zu produzieren, die mit sich under
der Welt im Reinen sind

Erziehung vor allem in der Schule dient vor Allem der Wissensvermittlung, die
auch wichtig ist.

Meiner Meinung nach aber wird die Persönlichkeitsentwicklung weitgehend
vernachlässigt

Wichtig ist:
- Wissen, wer man ist (was ist mir wichtig)
- Lernen, Auseinandersetzungen zu führen
- Lernen, andere Meinungen zu akzeptieren
- Den eigenen Körper kennenlernen und akzeptieren
- Freundschaft und Sexualität
- Archetypen des Mann- und Frauseins kennenlernen und integrieren
  - Kriegerin/Krieger, Liebhaberin/Liebhaber, Heilerin/Heiler, Königin/König, ...
  - Dazu gehört auch das Anerkennen aller sexuellen Ausrichtungen, wie hetero, schwul, lesbisch,
    und Genderausdrücke
   
Natürlich gibt es auch wichtige Wissensfächer, allen voran Lesen Schreiben und Rechnen
- Vieles, was wir in der Schule lernen, ist aber für das normale Leben nicht so wirklich relevant
- Das Wichtigste ist eigentlich, zu lernen, wir man sich Wissen aneignet, wie man Informationen
   einschätzt und wie man Entscheidungen in unserer verwirrenden Welt trifft
- Beispiele für Sachen die meiner Meinung nach nicht so wichtig sind:
  - Mathe: eigentlich alles, was über Grundrechenarten, Dreisatz, 
    Prozentrechnung, Wahrscheinlichkeitsrechnung und Statistik hinausgeht
  - Geschichte: Jahreszahlen sind nicht so wichtig wie das Erkennen von Zusammenhängen und das
    Wissen um die vielen schlimmen Dinge, die im Laufe der Geschichte passiert sind
  - Gemeinschaftskunde: eines der wichtigeren Fächer, würde ich sagen, weil es zum Diskutieren
    anregt
  - Deutsch: eigentlich alles ausser Lesen, Schreiben, Grammatik und der Fähigkeit, sich in Wort
    und Schrift auszudrücken, also z.B. ist Literaturdiskussion nicht essentiell
  - Kunst: ist eigentlich eher wichtig, weil es um die Frage geht, was schön ist, und was man 
    ohne Worte (bzw. zwischen den Zeilen) ausdrücken kann. Literaturdiskussion würde demnach eher 
    hierhin gehören
  - Naturwissenschaften (Physik, Chemie, Biologie): eigentlich alles ausser den grundlegenden
    Zusammenhängen

Alle, die Sachen, die ich hier als unwichtig für die Schule erklärt habe, sind
natürlich für viele Menschen sowohl privat als auch beruflich wichtig. Das will
ich auch gar nicht negieren; die Frage ist nur, ob es die wichtigsten Themen für
die Schule sind. Ich finde, dass freiwillige Fächer da mehr Sinn machen, um in
die Tiefe zu gehen, also neben Deutsch gäbe es dann ein freiwilliges Fach
Deutsch 2.0 oder so, in dem man die ganzen anderen Sachen machen kann, die
interessant sind. Dass würde natürlich bedeuten, dass es eine relativ frühe
Spezialisierung der Kinder auf bestimmte Themengebiete gäbe. Um das spätere
Umsatteln (z.B. für ein Studium) zu ermöglichen, sollte es Möglichkeiten geben,
bestimmte Themengebiete auch später anzugehen.

Das sind hier alles grobe Skizzen; es soll gar kein Anspruch auf Vollständigkeit
geben. Es gibt sicher auch viele Themen, die ich hier als wichtig angebe, die
nicht in der alleinigen Verantwortung ded Schulsystems liegen, aber meiner
Meinung nach läuft hier so viel schief, dass ein energisches Umsteuern notwendig
ist.
     
