# Ideen für politische Änderungen

Das sind Ideen für politische Ideen, die ich zum Teil schon eine Weile mit mir
herumtragen. Es sind Denkanstöße, die zur Diskussion anregen sollten. Das Ziel
ist nicht, irgendwelchen imaginären Feinden zu schaden, sondern vielmehr, das 
wirtschaftlich und politische Geschehen zum Nutzen der Allgemeinheit zu
gestalten.

## Umsatz besteuern

Probleme: Firmen verschieben ihre Verluste so, dass sie in den Ländern, in denen
sie viel Umsatz machen, keine Steuern bezahlen müssen.

Lösung: Wir besteuern auch direkt Umsätze, und zwar nicht wie bei der
Umsatzsteuer, dass man sie wieder raus bekommt. Allerdings sollte diese Steuer
sehr gering sein, und gestaffelt werden, um kleine Unternehmen nicht unnötig zu
belasten. Schliesslich geht es darum, Steuerflucht zu vermeiden, und das ist
haupotsächlich ein Problem bei größeren Firmen.


## Private Krankenversicherung in die gesetzliche integrieren

Problem: Gutverdienende nehmen nicht an der Verteilung der Lasten der Kosten auf
alle teil.

Lösung: Entweder die privaten Krankenversicherungen ins gesetzlichem Syste
einbinden, oder aber Teile der Krankenkassenkosten für die privaten in die
gesetzlichen Krankenkassen einfließen lassen.

Private Zusatzversicherungen sollten natürlich weiterhin möglich sein.


