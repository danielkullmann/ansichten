# Bedingungsloses Grundeinkommen (BGE)

Im Prinzip finde ich die Idee sehr gut; ich glaube, dass so Menschen unterstützt werden können,
ihrem Lebenstraum möglichst nah zu kommen, und Menschen können in ihren vielfältigen Rollen 
unterstützt werden, die sie im Leben annehmen, z.B. wenn sie Angehörige pflegen, oder einer
gemeinnützigen Tätigkeit nachgehen, oder einfach Kunst machen.

Trotzdem ist es wichtig, diesen Ansatz von allen Seiten zu prüfen. Den grundlegenden Einwand 
"dann arbeitet ja keiner mehr" sollte man nicht so einfach zur Seite wischen; es kann sehr wohl
sein, dass das Einführen eines BGE dazu führt, dass de Wirtschaftsleistung des Landes erstmal
deutlich einbricht. Und wie wird das BGE dann bezahlt?

