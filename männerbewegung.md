# Die Männerbewegung

Ich verstehe die Männerbewegung als logische Fortsetzung des Feminismus, und
keineswegs als Gegenbewegung dazu. ielmehr soll sie die Arbeit des Feminismus
vollenden, in dem sich auch die Männer von alten Rollenvorstellungen lösen.

Dazu gehören:

* verstehen, dass wir alle weibliche und männliche Anteile haben
* diese Anteile beziehen sich auf viele verschiedene Aspekte des Mannseins,
  z.B. angelegt an die Archetypen wie z.B. Krieger, Liebender, Magier, und König
* die richtige Mischung dieser Anteile für sich selbst herausfinden
* typische vorher als unmännlich angesehene Verhaltensweisen ohne Vorurteile betrachten:
  * das Zeigen von Gefühlen zulassen
  * die eigene Weichheit akzeptieren
