# Ansichten

- [Warum ich nicht an Gott glaube](gott.md)
- [Meine Grundgedanken](grundgedanken.md)

## Politik
- [Ideen für Änderungen](politik-ideen.md)
- [Bedingungsloses Grundeinkommen](grundeinkommen.md)
- [Freiheit vs. Sicherheit](freiheit-vs-sicherheit.md)
- [Wirtschaft](wirtschaft.md)
- [Sozialismus](sozialismus.md)
- [Ist Wohlstand nur eigener Verdienst?](./ist-wohlstand-nur-eigener-verdienst.md)
- [Bundestagswahl 2021](bundestagswahl2021.md)
- [Nachhaltigkeit, Umweltschutz, Klimaschutz](nachhaltigkeit.md)

## Spezielle Themen

- [Transgender](transgender.md)
- [Gendern](gendern.md)
- [Umgekehrte Diskriminierung](umgekehrte-diskriminierung.md)
- [Rassismus](rassismus.md)
- [Abtreibung](abtreibung.md)
- [Erziehung](erziehung.md)
- [Männerbewegung](männerbewegung.md)

## Bücherliste

* Frank Schätzing: Was, wenn wir einfach die Welt retten?
* Marco Bülow: Lobbyland


## Fehlende Seiten

- Protestwählen: warum nicht?
  - Wird ja oft als Argument dagegen gebracht, das man damit nur die "Gegner" stärkt
  - Wenn allerdings dan ganze politische System so kaputt ist, dass nichts mehr hilft,
    ist meiner Meinung nach Protestwählen schon akzeptabel, um den Finger mal in die Wunde zu legen
  - Die Leute, die dagegen argumentieren, verstehen wahrscheinich nicht, wie kaputt das System ist:
    - Politische Initiativen der politischen gegner werden prinzipiell abgelehnt, auch wenn sie gut sind
      (Stichwort Evakuierung der afghanischen Helfer)
    - Korruption wird akzeptiert (Amthor, Scholz, aufgeweichtes Lobbyregister)
- Aktienmärkte, Hedgefonds: Warum ich skeptisch bin
- Barack Obama (was ich schätze, was ich scheusslich finde)
  - Gut: Er konnte reden, er kam sympathisch rüber
  - Schlecht: Er hat Guantanamo Bay nicht geschlossen
  - Schlecht: Er hat die Kriege weitergeführt
  - Schlecht: er hat die Drone Strikes weitergeführt
  - Neutral: Er hat eine sehr konservative Reform des Gesundheitswesen durchgeführt
  - Er hat nicht wirklich was für die Reform des Justizwesens getan (criminal justice reform)
- Trump
  - Ich finde nicht alle seine Ideen schlecht
  - Als Person finde ich ihn schrecklich: Lügner, Hochstapler, Unterstützer von Rassisten
  - Viele seiner "policies" sind schrecklich
- Klimawandel
  - Technologie ja, aber nicht um jeden Preis
  - Kernenergie als Energielösung? Das kommt wieder mehr auf
    Ist es möglich, sichere Kernkraft zu haben?
- Einfache Lösungen
  - Komposttoiletten
  - Kleingärtnerei
  - Lokale Wirtschaftssysteme, die miteinander verknüpft sind
  - Nachhaltige(re) Transportsysteme: Kombination aus Schiene und Nahverkehr (öffentlich und persönlich)
- Technologiewahn (Hyperloop, Silicon Valley)
- Whistle Blowers (Julian Assange, Edward Snowden, ..)
- Corona
- Pharmaindustrie
- Großindustrie im Allgemeinen (warum Großunternehmen schädlich für die Gesellschaft sind)
- Wieso unregulierter Kapitalismus schlecht ist, aber Kapitalisten nicht unbedingt böse sind
- Umgang mit der AfD und mit Rassisten/Nazis
- Gendern
- Systemische vs. individuelle -ismen (z.B. Rassismus)
  Warum systemischer Rassismus nicht bedeuten muss, dass Individuen rassistisch sind
- Inzest: Warum Liebe zwischen Geschwistern nicht verboten sein sollte, solange dabei keine
  Machtspielchen damit verbunden sind
- Sport: Brauchen wir Leistungssport?
- Wissenschaft: Welche Forschung sollte wieviel gefördert werden?
- Kann man der Wissenschaft trauen, wenn sie eng mit der Wirtschaft verknüpft ist?
  - Welche Interessen werden verfolgt, vor allem welche monetären Interessen?
  - Die allermeisten Wissenschaftler sind einfach nur an der Forschung
    interessiert, daran, mehr über die Welt herauszufinden, und haben keine
    "niederen" Gründe. Wenn man Wissenschaft aber mit Wirtschaft verknüpft,
    werden die hehren Interessen oft durch die Gier ersetzt. Beispiel
    Tabakkonzerne: die wussten jahrzehntelang, dass Rauchen schädlich ist, haben
    diese Erkenntnisse aber verheimlicht. Beispiel Pharmakonzerne: diese
    Konzerne sind nicht der Gesundheit ihrer Kunden verpflichtet; dass ist nur
    ein Ziel, dass aus ihrem eigentlichen Interesse abgeleitet wird: dem Zwang
    zum Wachstum. Das ist noch nicht einmal die Schuld der Bosse, sondern kommt
    auch daher, dass ein aktiennotiertes Unternehmen verpflichtet ist, möglichst
    viel Gewinn zu erwirtschaften. Wenn sie das nicht tun, können sie von ihren
    Aktionären belangt werden. Also ist sämtliche Arbeit darauf auszurichten,
    was Profit bringt. Und wenn man dabei einmal "vergisst", über mögliche
    Risiken zu informieren gehört dazu. Wie kann das gelöst werden? Durch
    horrende Strafen, die den Konzernen angedroht werden, wenn sie sich nicht
    korrekt verhalten; dadurch verhalten sie sich nämlich kontraproduktiv zu dem
    Ziel, den Profit zu maximieren, und deshalb sind sie dann gezwungen, ihre
    Kunden ausreichend zu informieren. Das ist offensichtlich ein kaputtes System.
    (Als Randnotiz: wie steht die FDP zu dieser Einschränkung der
    unternehmerischen Freiheit?)
- der Bürger als Entität, die sich frei entscheiden können soll
  - Das ist ja das Gegenkonzept zu "übermässigen" Kontrollmaßnahmen der Politik:
    das ja jeder Bürger selbst entscheiden könnnen soll, was er tut. Wenn er sich
    genügend informiert hat, muss er ja wissen, welche Entscheidung er treffen muss.
    Nachteil: unsere Welt ist inzwischen so kompliziert, dass es fast unmöglich ist,
    sich in einem bestimmten Thema umfassend auszukennen.
- Korruptionsskandale in der Politik:
  - wäre gut, eine Liste von allen diesen Skandalen zu haben: Scholz, Amthor,
    Masken, Spendenskandel der CDU, AfD, etc.
