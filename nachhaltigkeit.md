# Nachhaltigkeit

Das umfasst viele Themen:

* Umweltschutz
* Klimaschutz
* Müllproblematik

Eine Sache, die man beim Thema nicht aus dem Blick verlieren sollte, ist die
Verhältnismässigkeit. Ist es verhältnismässig, dass für jede geschützte Tierart
extreme Maßnahmen getroffen werden, um sie zu schützen?

Beispiel: Durch das Habitat einer geschützten Mausart wird eine Autobahn gebaut.
Als Maßnahmen zum Schutz dieser Art wird enormer Aufwand getrieben. Ist dieser
Aufwand gerechtfertigt?

Zum Einen ist es natürlich sehr wichtig, geschützte Tierarten auch zu schützen.
Jede einzelne Tierart, die ausstirbt, hinterlässt Lücken im Ökosystem, die
irgendwie aufgefüllt werden (Beispiel: Ausrotten der Wölfe im Yellowstone 
Nationalpark (Quelle!)). Das kann zu enormen Veränderungen im ganzen Ökosystem
führen. Und im Prinzip ist Biodiversität wichtig, siehe Monokulturen.

Andererseits sterben Tierarten ja auch ganz natürlich aus, weil sich ihre Umwelt
ändert, und sich diese Tiere diesen Änderungen nicht (schnell genug) anpassen können
und deshalb aussterben. Dies mit allen Mitteln verhindern zu wollen ist ein
aussichtsloser Kampf. Dass viele dieser Änderungen menschengemacht sind, ist klar,
aber jede Art führt zu Veränderungen

