# Ist Wohlstand nur eigener Verdienst?

In der Diskussion um bessere soziale Leistungen wird ja häufig das Argument "Leistung muss sich wieder lohnen" gebracht. Darin ist die Ansicht versteckt, dass jeder, der sich anstrengt, es auch zu etwas bringt, und dass fehlender Wohlstand etwas mit Faulheit zu tun hat, während Wohlstand nur der eigenen Leistung zu verdanken ist. 

Diese ist meiner Meinung nach aus zwei Gründen grundlegend falsch. Der erste ist, das Erfolg im Leben sehr viel mit Glück zu tun hat. Es gibt ja nicht nur den einen Mark Zuckerberg, der die Idee hatte, ein soziales Netzwerk zu gründen. Diese Idee ist schon älter. Sein Glück war, dass er zur richtigen Zeit damit angefangen hat. Hat er hart gearbeitet? Ja, natürlich. Aber sein Erfolg hat auch sehr viel mit Glück zu tun. Es gibt ja sehr viele Leute, die auch gute Ideen haben, aber damit nicht erfolgreich werden.

Der zweite Grund ist, dass der Erfolg eines Unternehmens auch sehr viel mit der vorhandenen Infrastruktur zu tun hat. Wäre Facebook
in einem ländlichen Umfeld oder in einem dritte-Welt Land auch so erfolgreich geworden? Wahrscheinlich nicht; der Erfolg kommt ja auch daher, dass es dort, wo Facebook gegründet wurde, viel Infrastruktur gibt, die zum Erfolg beigetragen hat: Gut ausgebildete 
Menschen; Schulen und Universitäten, an denen diese Menschen ausgebildet wurden; Verkehrsinfrastrukut, die es erlaubt, bei Facebook zu arbeiten, auch wenn man weiter weg wohnt; Polizei, Feuerwehr und andere Einrichtungen der öffentlihen Sicherheit; ein Rechtsstaat, der Rechtssicherheit garantiert; usw.

Ich will gar nicht die persönliche Leistung von diesen Personen kleinreden. Ich will aber darauf hinweisen, dass diese Personen in besonderem Maße von der vorhandenen Infrastruktur profitiert haben und immer noch profitieren, und es deshalb auch selbstverständlich ist, dass sie mehr zu deren Erhalt und Ausbau beitragen als Andere.
