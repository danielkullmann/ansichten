# Rassismus

Struktureller Rassismus bedeutet nicht, dass jeder ein rassist ist. Es bedeutet, dass es interne Strukturen gibt,
die dazu führen, dass sich Menschen in bestimmten Situationen rassistisch verhalten, oft unbewusst.

Das Ziel muss sein, diesen strukturellen Rassismus langsam aus unserem System zu entfernen, indem wir die 
Mechanismen, mit denen er funktioneiert verstehen, und Gegenmechanismen entwerfen. Z.B. Bewerbungen soweit
wie möglich anonymisieren, so dass subtile Hinweise wie z.B. ein fremdartiger Name nicht dazu führt, dass 
wir die Bewerbung intuitiv schlechter bewerten.

Um dass in einen größeren Rahmen zu stellen, können wir uns die Menschheitsgeschichte anschauen: in vielerlei
Hinsicht haben sich unsere Gemeinschaften verbessert, z.B. waren früher grausame Strafen ganz normal; die 
Menschen fanden es ganz natürlich, dass einem Dieb die Hand abgehackt wird. Diese Einstellung ist langsam
aus unseren Gesellschaften verschwunden, und jetzt verstehen viele Leute nicht mehr, wie Menschen das früher
gut finden konnten. 

So ein Prozess wird auch der Rassismus durchmachen, wenn wir immer mehr Menschen mit anderem kulturellen 
Hintergrund kennenlernen.

