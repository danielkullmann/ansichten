# Bin ich ein Sozialist?

Ich würde erstmal "Nein" sagen, weil ich gar nicht genau weiss, was das bedeuten soll,
und weil dieser Begriff ja gar nicht klar definiert ist.

Ich würde das eher an ganz bestimmten Aussagen festmachen, zu denen ich dann Stellung beziehen kann.

Also z.B.

- Sollten Arbeiter ihre Produktionsmittel selbst besitzen? => Ja, finde ich generell gut
- Sollten alle Firmen enteignet und zu staatlichen Firmen werden? => Nein! Es sollte eher
  so etwas wie Kooperativen geben. Wir sollten Enteignungen auch möglichst vermeiden, und eher
  einen langsamen Umbau des Wirtschaftssystems anstreben, indem wir alternative Eigentumsmodelle
  stärker fördern als die klassischen kapitalistischen

Für mich bedeutet Soialismus zuerst einmal, solidarisch zu handeln und die Schwachen zu unterstützen,
so dass sie möglichst wieder selbst auf die Beine kommen können. Und wenn das die Definition von 
Sozialismus ist, dann bin ich auch ein Sozialist!
