# Warum ich nicht an Gott glaube

## TLDR

Als Kind habe ich mir die Frage gestellt, ob ich an Gott glaube. Nach einigem Überlegen und auch Beten
bin ich dazu gekommen, dass ich es nicht tue.

Danach habe ich mir sehr lange im Prinzip keine Gedanken darüber gemacht. Erst sehr viel später bin ich auf die
Atheisten-Bewegung auf Youtube gestoßen, und habe angefangen, mir dazu mehr Gedanken zu machen. 

## Die Kirche

Die beiden grossen Kirchen in Deutschland, die katholische und die evangelische, sind meiner Meinung nach
furchtbare Organisationen; bei der evangelischen bin ich mir nicht sicher. gerade bei den vielen Missbrauchsskandalen
hat die katholische Kirche vollkommen versagt. Ich verstehe auch nicht, wieso die Priester nicht einfach
vor ein Gericht gestellt werden können; sie mögen vielleicht Vertreter eines Gottes sein, aber sie sind immer
noch Menschen und ganz normale Bürger dieses Landes.

Auf jeden fall sollte die Kirchensteuer abgeschafft werden; Kirchen sollten, wie jeder andere Verein auch,
ihre Mitgliedergebühren selbst eintreiben. Sie wissen ja noch nicht einmal, wer alles Mitglied bei ihnen ist!

## Apologetics

## Kalaam Cosmological Argument

Finde ich nicht sehr überzeugend. Ausserdem kommt amn damit nicht sehr weit, sondern nur zu einem Grund, 
den es dann geben muss. Über diesen Grund kann man aber ehrlicherweise gar nichts aussagen, weil er absolut
ausserhalb unseres Universums, auserhalb von Raum und Zeit, "existiert" (Existenz ist ja schonmal der falsche
Begriff dafür, weil wir damit zeitlich und räumliche identifizierbare Dinge meinen)

## Objektive Moral

Gibt es die?

Wenn ein Gott uns die Moral vorgibt, dann ist sie ja subjektiv in Beziehung zu diesem Gott.

Wenn es eine gäbe, wieso haben sich die Moralvorstellungen im Laufe der Zeit so sehr geändert? Und was
ist dann die richtige Moral? Haben wir sie verbessert oder verschlimmert?


