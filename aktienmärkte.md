# Aktienmärkte

## Die Aktienmärkte sind zu einem Spielplatz von Spekulanten geworden

Die ursprüngliche Idee, dass sich Firmen über die Ausgabe von Aktien mit Geld
versorgen können, tritt immer mehr in den Hintergrund. Die Spekulation mit Aktien,
insbesondere Leerverkäufe, High-Frequency Trading und Manipulationen von Aktienpreisen
haben mit diesem ursprünglichen Ziel sehr wenig zu tun.

Der Wert einer Aktie hat sehr häufig wenig mit dem eigentlichen wirtschaftlichen Wert
des Unternehmens zu tun, sondern ist ein sehr psychologischer Wert, der sich sehr schnell
ändern kann.

Aus diesem Grund ist z.B. auch das Vermögen von Menschen wie z.B. Jeff Bezos als viel 
geringer einzuschätzen. Sollte er irgendwann mal damit anfangen, Aktien in grossem Umfang
zu verkaufen, würde der Kurs dieser Aktien (Amazon hauptsächlich) schnell sinken.

## Was sollte der Sinn von Aktienmärkten sein?

- Firmen mit Geld versorgen
- Anleger können sich an Firmen beteiligen und an deren Erfolg teilhaben, über die Dividende und
  den Kurs der Aktie

## Erfüllen sie diesen Zweck?

## Wie können wir die Börse zu einem wertvollen Teilnehmer des Marktes machen?

Mit einer Transaktionssteuer! 
