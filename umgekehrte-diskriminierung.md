# Ist Umgekehrte Diskriminierung gut oder schlecht?

Wie sonst soll man strukturelle Ungleichheiten ausgleichen?

Kann man dazu nicht eine schöne Analogie finden?

Ich kann verstehen, wenn Menschen das ablehnen, aber wenn wir keine
andere Lösung zum Umkehren von Diskriminierung finden, ist das immer
noch besser, als die ursprüngliche Diskriminierung beizubehalten.

Es geht ja auch gar nicht darum, Menschen z.B. nach Hautfarbe oder Abstammung
zu sortieren und der Gruppe, die man als benachteiligt ansieht, Vorteile zu
verschaffen. Im Gegenteil sollte man versuchen, für alle Benachteiligte, also z.B.
auch weiße Deutsche, Verbesserungen des Lebens anzustreben.

Die sogenannten "bildungsfernen" Schichten haben ja in Deutschland auch sehr mit Nachteilen zu kämpfen; die soziale Durchlässigkeit in Deutschland ist ja sehr gering.

Natürlich bedeutet dass dann auch, das bestimmte andere "Klassen", also in dem Fall die Wohlhabenden,
etwas mehr abgeben müssen. Aber das sollte eigentlich auch selbstverständlich sein ([Ist Wohlstand nur eigener Verdienst?](./ist-wohlstand-nur-eigener-verdienst.md)).
